import numpy as np
import matplotlib.pyplot as plt
import sys
import datetime
import os
from scipy import signal
day=datetime.date.today()
y=day.year
m=day.month
d=day.day

data_dir="/data_small/noisy_train_small/"         # directory containing data files
output_dir="output/"
file=sys.argv[1]

data=np.loadtxt(data_dir+file)
(n,p)=np.shape(data)
wl=input(" WL number : ")

# Moyenne
avg=0
for j in range(p):
    avg+=data[int(wl),j]
avg=avg/p
print(avg)


# Liste d'acquisition
acq_data=[]
offset=[]
for i in range(0,p):
    I=data[int(wl),i]
    if I > avg:
        acq_data.append(I)
        offset.append(avg)



os.chdir(os.getcwd()+"/"+output_dir)
plt.figure()
plt.plot(acq_data,label="wl nbr"+str(wl))
plt.plot(offset, label="avg")
plt.legend()
plt.xlabel("time")
plt.ylabel("flux")
plt.savefig(str(y)+"-"+str(m)+"-"+str(d)+"_"+file[0:-4]+".png")
plt.show()

#filtre de butterworth
fe=1
f_nyq=fe/2
fc=0.1

b, a = signal.butter(4,fc/f_nyq, 'low', analog=False)
acq_but = signal.filtfilt(b,a,acq_data)

plt.figure()
plt.plot(acq_but,label="acq filtrée")
plt.legend()
plt.xlabel("time (en Te)")
plt.ylabel("flux")
plt.savefig(str(y)+"-"+str(m)+"-"+str(d)+"_"+"filtré"+".png")
plt.show()


_max=[acq_but[0]]
_min=[acq_but[0]]
for i in range(1,len(acq_but)-1):
    if acq_but[i] >= acq_but[i+1] :
        if acq_but[i] >= acq_but[i-1] : 
            _max.append(acq_but[i])
            _min.append(_min[-1])
        else: 
            _max.append(_max[-1])
            _min.append(_min[-1])
    elif acq_but[i] <= acq_but[i+1]:
        if acq_but[i] <= acq_but[i-1] : 
            _min.append(acq_but[i])
            _max.append(_max[-1])
        else: 
            _min.append(_min[-1])
            _max.append(_max[-1])



plt.figure()
#plt.plot(acq_but,label="acq filtrée")
plt.subplot(211)
plt.plot(_max,label="max")
plt.plot(_min,label="min")
plt.xlabel("time (en Te)")
plt.ylabel("flux")
plt.legend()
plt.subplot(212)
plt.plot(_max,color="green",label="max")
plt.plot(_min,color="blue",label="min")
plt.xlabel("time (en Te)")
plt.ylabel("flux")
plt.legend()
plt.savefig(str(y)+"-"+str(m)+"-"+str(d)+"_"+"max_min"+".png")
plt.show()


