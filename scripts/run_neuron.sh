#!/bin/bash 


clear
COLUMNS=$(tput cols) 

title="\033[33mLaunching neuron_ariel.py program for Ariel ML Data Challenge \033[0m" 
echo -e $title 
date | fmt -c -w $COLUMNS

DATE=$(date +%Y_%m_%d)
log_file=log_ex_$1.txt

echo  >> $log_file
echo /======================================================================================='\'>> $log_file
echo Elapsed time log for neuron_ariel.py >> $log_file
echo Date  : $(date) >> $log_file
echo Made by Clément Bourretère at 6/5/22 >> $log_file
echo "|=======================================================================================|">> $log_file
d1=$(date)
t1=$(date +"%s")
echo Date at the beginning of python" ":$d1 >> $log_file
python neuron_ariel.py $1
d2=$(date)
t2=$(date +"%s")
echo Date at the end of python"       ": $d2 >> $log_file
echo Elapsed time "in sec""          ": $(($t2-$t1)) >> $log_file
echo '\'=======================================================================================/>> $log_file

mv ./log.txt ../output/$DATE/$log_file