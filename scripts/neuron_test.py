import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from ariel_student import *
import random as rd
import os
from datetime import datetime
from statistics import mean
import sys
import neuron_ariel as na


if __name__ == '__main__':
    DATA_DIR = '/Users/clementbourretere/ARIEL/data_small/' 
    OUT_DIR  = '/Users/clementbourretere/ARIEL/ariel-deep-learning/output/'
    MODEL_PATH = '/Users/clementbourretere/ARIEL/ariel-deep-learning/model/model.pt'

    # Data analyse 
    dir = "test"
    test_obs_path = os.path.join(DATA_DIR, 'noisy_test_small.txt')

    model = na.Net4()
    model.load_state_dict(torch.load(MODEL_PATH))
    model.eval()

    list_test = []
    with open(test_obs_path, 'r') as f:
        for i, line in enumerate(f.readlines()):
            file_type, file_name = line.replace('\n','').split('/')
            list_test.append(file_name[0:-4])
            
    
    na._store_()
    if os.path.exists("rr_neuron.txt") : os.remove("rr_neuron.txt")
    file=open("rr_neuron.txt", 'w')
    for name in tqdm(list_test, desc="Test data analysis", bar_format='{l_bar}{bar:20}{r_bar}{bar:-10b}'):
        data  = load_lightcurve(name, dir)
        edge  = torch.from_numpy(data['light_curve'][20:20+na.N]).float()
        trans = torch.from_numpy(data['light_curve'][int(130-na.N/2):int(130+na.N/2)]).float()
        edge.sort()
        trans.sort()
        rr_pred = model(edge,trans)
        rr_pred = rr_pred.detach().numpy()
        
        for wl in range(54):
            file.write(str(rr_pred[wl])+'\t')
        file.write(str(rr_pred[-1])+'\n')
    file.close()
