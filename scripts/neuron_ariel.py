
import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from ariel_student import *
import random as rd
import os
from datetime import datetime
from statistics import mean
import sys

def _store_():
    date = datetime.now().strftime("%Y_%m_%d-%I:%M:%S_%p")[0:10]
    os.chdir(OUT_DIR)
    if not os.path.isdir(date):
        os.mkdir(date)
    os.chdir(date)
    return True

N = 60 # window's size
class Net1(nn.Module):
    def __init__(self):
        super(Net1, self).__init__()
        self.linears1 = nn.ModuleList([nn.Linear(N, 1) for i in range(55)])
        self.linears2 = nn.ModuleList([nn.Linear(N, 1) for i in range(55)])
        self.linears3 = nn.ModuleList([nn.Linear(2, 1) for i in range(55)])
        self.layer   = nn.Linear(55, 55)

    def forward(self, edge, trans):
        P = torch.from_numpy(np.array([]))
        for i, l  in enumerate(self.linears1):
            _edge = self.linears1[i](edge[:,i])  
            _edge = F.relu(_edge)
            

            _trans = self.linears2[i](trans[:,i])
            _trans = F.relu(_trans)
            

            p = torch.cat((_edge,_trans), 0)
            p = self.linears3[i](p)
            p = F.relu(p)
            
            P = torch.cat((P.float(),p.float()), 0)
        
        P = self.layer(P)
        P = F.relu(P)
        return P

class Net2(nn.Module):
    def __init__(self):
        super(Net2, self).__init__()
        self.linears1 = nn.ModuleList([nn.Linear(N, 1) for i in range(55)])
        self.linears2 = nn.ModuleList([nn.Linear(N, 1) for i in range(55)])
        self.linears3 = nn.ModuleList([nn.Linear(2, 1) for i in range(55)])
        self.layer1   = nn.Linear(55, 55)
        self.layer2   = nn.Linear(55, 55)

    def forward(self, edge, trans):
        P = torch.from_numpy(np.array([]))
        for i, l  in enumerate(self.linears1):
            _edge = self.linears1[i](edge[:,i])  
            _edge = F.relu(_edge)
            

            _trans = self.linears2[i](trans[:,i])
            _trans = F.relu(_trans)
            

            p = torch.cat((_edge,_trans), 0)
            p = self.linears3[i](p)
            p = F.relu(p)
            
            P = torch.cat((P.float(),p.float()), 0)
        
        P = self.layer1(P)
        P = F.relu(P)
        P = self.layer2(P)
        P = F.relu(P)
        return P

class Net3(nn.Module):
    def __init__(self):
        super(Net3, self).__init__()
        self.linears1 = nn.ModuleList([nn.Linear(N, 32) for i in range(55)])
        self.linears2 = nn.ModuleList([nn.Linear(N, 32) for i in range(55)])
        self.linears3 = nn.ModuleList([nn.Linear(64, 32) for i in range(55)])
        self.layer1   = nn.Linear(32*55, 16*55)
        self.layer2   = nn.Linear(16*55, 8*55)
        self.layer3   = nn.Linear(8*55, 1*55)

    def forward(self, edge, trans):
        P = torch.from_numpy(np.array([]))
        for i, l  in enumerate(self.linears1):
            _edge = self.linears1[i](edge[:,i])  
            _edge = F.relu(_edge)

            _trans = self.linears2[i](trans[:,i])
            _trans = F.relu(_trans)

            p = torch.cat((_edge,_trans), 0)
            p = self.linears3[i](p)
            p = F.relu(p)
            P = torch.cat((P.float(),p.float()), 0)

        P = self.layer1(P)
        P = F.relu(P)
        
        P = self.layer2(P)
        P = F.relu(P)
        
        P = self.layer3(P)
        P = F.relu(P)
        
        return P

class Net4(nn.Module):
    def __init__(self):
        super(Net4, self).__init__()
        self.linears1 = nn.ModuleList([nn.Linear(N, N) for i in range(55)])
        self.linears2 = nn.ModuleList([nn.Linear(N, N) for i in range(55)])
        self.linears3 = nn.ModuleList([nn.Linear(N, 32) for i in range(55)])
        self.linears4 = nn.ModuleList([nn.Linear(N, 32) for i in range(55)])
        self.linears5 = nn.ModuleList([nn.Linear(64, 32) for i in range(55)])
        self.layer1   = nn.Linear(32*55, 16*55)
        self.layer2   = nn.Linear(16*55, 8*55)
        self.layer3   = nn.Linear(8*55, 4*55)
        self.layer4   = nn.Linear(4*55, 55)

    def forward(self, edge, trans):
        P = torch.from_numpy(np.array([]))
        for i, l  in enumerate(self.linears1):
            _edge = self.linears1[i](edge[:,i])  
            _edge = F.relu(_edge)
            _edge = self.linears3[i](edge[:,i])  
            _edge = F.relu(_edge)

            _trans = self.linears2[i](trans[:,i])
            _trans = F.relu(_trans)
            _trans = self.linears4[i](trans[:,i])
            _trans = F.relu(_trans)

            p = torch.cat((_edge,_trans), 0)
            p = self.linears5[i](p)
            p = F.relu(p)
            P = torch.cat((P.float(),p.float()), 0)

        P = self.layer1(P)
        P =F.relu(P)
        P = self.layer2(P)
        P = F.relu(P)
        P = self.layer3(P)
        P =F.relu(P)
        P = self.layer4(P)
        P = F.relu(P)
        return P




if __name__ == '__main__':
    
    if (sys.argv[1]=="1"): net = Net1()
    elif (sys.argv[1]=="2"): net = Net2()
    elif (sys.argv[1]=="3"): net = Net2()
    elif (sys.argv[1]=="4"): net = Net4()
    else : net = Net1()
    
    optimizer = torch.optim.Adam(net.parameters(), lr=0.0001)
    loss_func = torch.nn.MSELoss()

    dir = 'train'
    DATA_DIR = '/Users/clementbourretere/ARIEL/data_small/' # Contient 6704=16*419 fichiers de 419 planètes
    OUT_DIR  = '/Users/clementbourretere/ARIEL/ariel-deep-learning/output/'
    EPOCH = 6

    _show_ = True
    heure = datetime.now().strftime("%H:%M")
    dataset, L, list_obs, data_test, G = [], [], [], [], []
    L2, G2 =  [], []

    list_obs_path = os.path.join(DATA_DIR, f'noisy_{dir}_small.txt')
    with open(list_obs_path, 'r') as f:
        for i, line in enumerate(f.readlines()):
            file_type, file_name = line.replace('\n','').split('/')
            list_obs.append(file_name[0:-4])

    _full_ = False
    if _full_ :
        list_train = list_obs
        train_obs_nb = len(list_train)
    else :
        train_obs_nb = 20*16
        list_train = list_obs[0:train_obs_nb]

    test_obs_nb = 5*16
    list_test = list_obs[train_obs_nb+1:test_obs_nb+train_obs_nb+1]

    #for i in range(test_obs_nb):
    #    list_test.append(list_obs[rd.randint(0,len(list_obs)-1)])
    
    
    for epoch in tqdm(range(EPOCH), desc="data training", bar_format='{l_bar}{bar:20}{r_bar}{bar:-10b}'):
        for name in list_train:
        #for name in tqdm(list_train, desc="training data loading", bar_format='{l_bar}{bar:20}{r_bar}{bar:-10b}'):
            data  = load_lightcurve(name)
            rr    = torch.from_numpy(data['relative_radii']).float()
            edge  = torch.from_numpy(data['light_curve'][20:20+N]).float()
            trans = torch.from_numpy(data['light_curve'][int(130-N/2):int(130+N/2)]).float()
            edge.sort()
            trans.sort()

            rr_pred = net(edge,trans)
            loss = loss_func(rr_pred, rr)
            optimizer.zero_grad()
            loss.backward()        
            optimizer.step()
                
            loss = float(loss.detach().numpy())
            L2.append(loss)
        L.append(mean(L2))
        L2 = []

        net.eval()
        with torch.no_grad():
            for name in list_test:
            #for name in tqdm(list_test, desc=" testing data loading", bar_format='{l_bar}{bar:20}{r_bar}{bar:-10b}'):   
                data  = load_lightcurve(name)
                rr    = torch.from_numpy(data['relative_radii']).float()
                edge  = torch.from_numpy(data['light_curve'][20:20+N]).float()
                trans = torch.from_numpy(data['light_curve'][int(130-N/2):int(130+N/2)]).float()
                edge.sort()
                trans.sort()
                rr_pred = net(edge,trans)
                     
                g = loss_func(rr_pred, rr)
                g = float(g.detach().numpy())
                G2.append(g)
            G.append(mean(G2))
            G2 = []
        net.train()

    

    plt.figure(figsize=(8, 6))
    plt.title("Loss function (ISF - Input Sorted Flux) \n Number of observations trained = "+str(train_obs_nb)+"\n Number of observations tested = "+str(test_obs_nb))
    plt.plot(L)
    plt.xlabel("index")
    if _store_():
        plt.savefig("Loss_function_Net"+sys.argv[1]+"_"+heure+".png")
    if _show_: 
        plt.show()

    plt.figure(figsize=(8, 6))
    plt.title(r"Transit depth error $\epsilon_{rr}$ (ISF - Input Sorted Flux)"+"\n Number of observations trained = "+str(train_obs_nb)+"\n Number of observations tested = "+str(test_obs_nb))
    plt.plot(G)
    plt.xlabel("index")
    plt.ylabel(r'$\epsilon_{rr}\ (w_i)$')
    if _store_():
        plt.savefig("rr_error_Net"+sys.argv[1]+"_"+heure+".png")
    if _show_: 
        plt.show()

    """
    # Data analyse 
    dir = "test"
    test_obs_path = os.path.join(DATA_DIR, 'noisy_test_small.txt')

    list_test = []
    with open(test_obs_path, 'r') as f:
        for i, line in enumerate(f.readlines()):
            file_type, file_name = line.replace('\n','').split('/')
            list_test.append(file_name[0:-4])
            
    
    _store_()
    if os.path.exists("rr_neuron.txt") : os.remove("rr_neuron.txt")
    file=open("rr_neuron.txt", 'w')
    for name in tqdm(list_test, desc="Test data analysis", bar_format='{l_bar}{bar:20}{r_bar}{bar:-10b}'):
        data  = load_lightcurve(name, dir)
        edge  = torch.from_numpy(data['light_curve'][20:20+N]).float()
        trans = torch.from_numpy(data['light_curve'][int(130-N/2):int(130+N/2)]).float()
        edge.sort()
        trans.sort()
        rr_pred = net(edge,trans)
        rr_pred = rr_pred.detach().numpy()
        
        for wl in range(54):
            file.write(str(rr_pred[wl])+'\t')
        file.write(str(rr_pred[-1])+'\n')
    file.close()
    """



            
    
    






    
          

            

    