import os
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from math import *


DATA_DIR = '/Users/clementbourretere/ARIEL/data_small/'
OUT_DIR  = '/Users/clementbourretere/ARIEL/ariel-deep-learning/output/'

_affichage_ = False
_print_ = False

def load_lightcurve(name, dir='train'):
    """Load all the data for an observation given it's name

    params :
        name (str) : AAAA_BB_CC,    AAAA (0001 to 2097) is an index for the planet observed, 
                                    BB (01 to 10) is an index for the stellar spot noise instance observed 
                                    CC (01 to 10) is an index for the gaussian photon noise instance observed.

        dir (str) : 'train' or 'test'
    return : 
        data (dictionary) : keys are 'star_temp', 'star_logg', 'star_rad', 'star_mass', 'star_k_mag', 'period', 'light_curve'
                             and also 'sma', 'incl', 'relative_radii' if training sample
    """
    assert dir in ['train', 'test']
    
    data = {}                           # dictionnaire vide
    data['observation'] = name
    
    # Load the light curve file
    data_path = os.path.join(DATA_DIR, f'noisy_{dir}_small', f'{name}.txt')
    light_curve = []
    with open(data_path, 'r') as f:
        for i, line in enumerate(f.readlines()):
            if i < 6:
                line_elt = line.split(':')
                data[line_elt[0][2:]] = float(line_elt[1][1:])
            else:
                light_curve.append(line.replace('\n', '').split('\t'))
    data['light_curve'] = np.array(light_curve).astype(float).T

    # If training sample, also load the target file
    if dir == 'train':
        relative_radii = []
        data_path = os.path.join(DATA_DIR, f'params_{dir}_small', f'{name}.txt')
        with open(data_path, 'r') as f:
            for i, line in enumerate(f.readlines()):
                if i < 2:
                    line_elt = line.split(':')
                    data[line_elt[0][2:]] = float(line_elt[1][1:])
                else:
                    relative_radii = line.replace('\n', '').split('\t')
        data['relative_radii'] = np.array(relative_radii).astype(float)

    return data

def plot_light_curves(light_curve, relative_radii, sample_name=''):
    T, W = light_curve.shape
    mean_light_curve = np.mean(light_curve, axis=1)
    std_light_curve = np.std(light_curve, axis=1)
    low = mean_light_curve - std_light_curve
    high = mean_light_curve + std_light_curve
    plt.errorbar(np.arange(T), mean_light_curve, yerr=std_light_curve, alpha=.5, fmt='-', linewidth=2, elinewidth=0.5, capsize=2, capthick=0.5)
    plt.fill_between(x=np.arange(T), y1=low, y2=high, alpha=.25)
    plt.ylim(low.min(), high.max())
    plt.title('Mean lightcurve {}.\nMean Target : {:.4f}'.format(sample_name, np.mean(relative_radii)))
    plt.show()

######################################################################################################
def moy(L):
    N=len(L)
    sum=0
    for i in L:
        sum+=i
    return(sum/N)

def med(L):
    N=len(L)
    if (N%2==0):
        return ( (L[int(N/2-1)]+L[int(N/2+1)])/2 )
    else :
        return L[int( (N+1)/2 )]

def noise_filter(name, wl=1, dir='train', window=60):
    sample = load_lightcurve(name, dir)
    ref=sample['light_curve'][20:20 + window, wl]
    ref.sort()
    trans=sample['light_curve'][20:-20, wl][int(130-window/2) : int(130+window/2)]
    trans.sort()
    gap=[ref[i]-trans[i] for i in range(len(trans))]
    
    if _affichage_ :

        os.chdir(OUT_DIR)
        
        plt.figure()
        plt.suptitle("Noise distribution on transit, obs_name ="+name+", wl= "+str(wl)+", window= "+str(window))

        ax1 = plt.subplot(321)
        ax1.plot(ref,color="red")
        ax1.set_title(' Edge Noise', y=0.8, loc='left')
        ax1.grid()

        ax2 = plt.subplot(322)
        ax2.plot(trans,color="green")
        ax2.set_title(' Transit Noise', y=0.8, loc='left')
        ax2.grid()

        ax3 = plt.subplot(323)
        ax3.plot(trans,color="green")
        ax3.plot(ref,color="red")
        ax3.set_title(' Noise on both', y=0.8, loc='left')
        ax3.grid()

        ax4 = plt.subplot(324)
        ax4.plot(gap,color="gold")
        ax4.set_title(' Transit depth',y=0.8, loc='left')
        ax4.grid()
        
        plt.savefig("noise_filter_caracteristic.png")
        plt.show()
        
    
    gap_moy = abs(moy(gap))
    gap_med = abs(med(gap))
    rr,e_moy,e_med=0,0,0

    if dir=='train':
        rr = sample['relative_radii'][wl]
        e_moy = abs( rr-sqrt(gap_moy) )/rr*100
        e_med = abs( rr-sqrt(gap_med) )/rr*100
    

    if _print_:
        print("\n")
        print("relative_radii = " + str( rr ) + "\n" )
        print("Gap moyen  = "  + str( sqrt(gap_moy) ) + " | e_moy =  " + str(round(e_moy,4)) + " %" + "\n" )
        print("Gap median = "  + str( sqrt(gap_med) ) + " | e_med =  " + str(round(e_med,4)) + " %" + "\n" )
    
    return(rr, gap_moy, gap_med, e_moy, e_med)
    
def transit_detector(name, wl, dir='train'):
    sample = load_lightcurve(name, dir)
    sample = sample['light_curve'][20:-20, wl]
    mid    = int(len(sample)/2)
    moy_wi = []
    for wi in range( 2, mid):
        t_sample = sample[mid-wi:mid+wi]
        t_moy    = moy(t_sample)
        moy_wi.append(t_moy)
    return(moy_wi)
     
def file_register(dir='train'):
    file_reg,liste  = [], [False]
    liste_txt = os.path.join(DATA_DIR, f'noisy_{dir}_small.txt')

    with open(liste_txt, 'r') as f:
        for i, line in enumerate(f.readlines()):
            file_type, file_name = line.replace('\n','').split('/')
            if int(file_name[0:4]) != liste[-1]:
                liste.append(int(file_name[0:4]))

        print(liste)
        obs=int(input("Which observation (integer) : "))
        assert obs in liste,"\n obs is not an available observation !"

    with open(liste_txt, 'r') as f:
        for i, line in enumerate(f.readlines()):
            file_type, file_name = line.replace('\n','').split('/')
            if int(file_name[0:4]) == obs:
                file_reg.append(file_name[0:-4] )
    return file_reg


if __name__ == '__main__':

    name = "0059_04_02"
    ex = 4
    
    
    if ex == 5 :
        os.chdir(OUT_DIR)
        start_time = time.time()
        if os.path.exists('relative_radi_test_moy.txt') : os.remove('relative_radi_test_moy.txt')
        if os.path.exists('relative_radi_test_med.txt') : os.remove('relative_radi_test_med.txt')
        file_moy=open('relative_radi_test_moy.txt', 'w')
        file_med=open('relative_radi_test_med.txt', 'w')
        dir = 'test'
        liste_txt = os.path.join(DATA_DIR, f'noisy_{dir}_small.txt')
        with open(liste_txt, 'r') as f:
            for i, line in enumerate(f.readlines()):
                file_type, file_name = line.replace('\n','').split('/')
                for wl in range(54):
                    (rr, gap_moy, gap_med, e_moy, e_med) = noise_filter(file_name[0:-4], wl, dir='test', window=30)
                    file_moy.write(str(sqrt(gap_moy))+'\t')
                    file_med.write(str(sqrt(gap_med))+'\t')
                (rr, gap_moy, gap_med, e_moy, e_med) = noise_filter(file_name[0:-4], 54, dir='test', window=30)
                file_moy.write(str(sqrt(gap_moy))+'\n')
                file_med.write(str(sqrt(gap_med))+'\n')
        file_moy.close()
        file_moy.close()
        elapsed_time = time.time() - start_time
        print('elapsed time for 2880 files : ', elapsed_time)


    if ex == 4 :
        os.chdir(OUT_DIR)
        plt.figure()
        plt.suptitle("Effect of window's size on transit depth, obs_name= "+name)
        plt.title("Mean = fct(window,wavelength) ")
        moy_transit_detection = [0]*128
        for wl in range(55):
            plt.plot(transit_detector(name, wl))
        plt.grid()
        plt.xlabel("window's size")
        plt.ylabel(r"$\overline{I}_{trans}$ - transit mean")
        plt.savefig("moy_fct_window.png")
        plt.show()
        
    if ex == 3 :
        os.chdir(OUT_DIR)
        plt.figure()
        plt.title("Observation : "+name+"\n"+"med & moy gap / rr (%)")
        E_moy = []
        E_med = []
        wi=30
        for wl in range(55):
            (rr, gap_moy, gap_med, e_moy, e_med) = noise_filter(name, wl, window=wi)
            E_moy.append(e_moy)
            E_med.append(e_med)
        plt.plot(E_moy, label="moy_"+str(wi))
        plt.plot(E_med, label="med_"+str(wi))
        plt.xlabel("Wavelength number")
        plt.ylabel("%")
        plt.grid()
        plt.legend()
        plt.savefig("med_moy_gap_"+name+".png")
        plt.show()

    if ex == 2 :
        os.chdir(OUT_DIR)
        plt.figure()
        plt.suptitle(r"Transit depth relative error $\epsilon_{rr\_\%}$")
        wi=[20,50]
        c1=["crimson","orange"]
        c2=["darkgreen","chartreuse"]
        plt.subplot(121)
        plt.title("window size= "+str(wi[0]))
        E_moy = []
        E_med = []
        for wl in range(55):
            (rr, gap_moy, gap_med, e_moy, e_med) = noise_filter(name, wl, window=wi[0])
            E_moy.append(e_moy)
            E_med.append(e_med)
        plt.plot(E_moy, label= "moy_"+str(wi[0]), color= c1[0])
        plt.plot(E_med, label= "med_"+str(wi[0]), color= c1[1])
        plt.xlabel(r'$w_i$ - wavelength' )
        plt.ylabel(r'$\epsilon_{rr\_\%}\ (w_i)$' )
        plt.grid()
        plt.legend()
        plt.subplot(122)
        plt.title("window size= "+str(wi[0]))
        E_moy = []
        E_med = []
        for wl in range(55):
            (rr, gap_moy, gap_med, e_moy, e_med) = noise_filter(name, wl, window=wi[1])
            E_moy.append(e_moy)
            E_med.append(e_med)
        plt.plot(E_moy, label= "moy_"+str(wi[1]), color= c2[0])
        plt.plot(E_med, label= "med_"+str(wi[1]), color= c2[1])
        plt.xlabel(r'$w_i$ - wavelength' )
        plt.ylabel(r'$\epsilon_{rr\_\%}\ (w_i)$' )
        plt.grid()
        plt.legend()
        plt.savefig("med_moy_gap.png" )
        plt.show()
    
    
    if ex == 1 :
        file_reg = file_register()
        if not __debug__:
            print(FILE_DIR)
            print(DATA_DIR)
            print(file_reg)
        for test in file_reg:
            sample = load_lightcurve(test.format(), 'train')
            plot_light_curves(sample['light_curve'][20:-20], sample['relative_radii'], sample['observation'])
    
    
    

